package test.java.jiratests.data;

public class Ticket {
    public String projectKey = "tst";
    public String projectName = "A Test Project";

    public String ticketKey = "imp";
    public String ticketType = "Improvement";

    public String summary = "Lorem ipsum dolor sit amet, mucius oblique ut sit.";

    public String assigneeKey = "una";
    public String assigneeName = "Unassigned";

    public String description = "Iusto affert an eum. Et eam suas copiosae scribentur. Ignota suavitate suscipiantur est ex, duo munere veritus accusata id, modo modus incorrupte eum ne. Eu vix melius euismod, nam ex enim cibo. Ne pri illud eligendi detraxit, solum convenire ex qui. Qui oratio torquatos ut, malis clita vituperatoribus duo cu. Mea mundi recteque at.";
}
