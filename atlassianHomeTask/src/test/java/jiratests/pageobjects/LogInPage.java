package test.java.jiratests.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogInPage {
    private final WebDriver driver;

    @FindBy(xpath = "//input[@id='username']")
    private WebElement logInInput;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//input[@id='login-submit']")
    private WebElement signIn;

    public LogInPage(WebDriver d) {
        this.driver = d;
        PageFactory.initElements(driver, this);
    }

    public TestProjectPage loginAs(String login, String password) {
        logInInput.sendKeys(login);
        passwordInput.sendKeys(password);
        signIn.click();
        return new TestProjectPage(driver);
    }
}

