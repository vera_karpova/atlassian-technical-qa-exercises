package test.java.jiratests.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import test.java.jiratests.utilities.Utils;

public class CreateTicketForm {
    private final WebDriver driver;

    private String projectInputXpath = "//input[@id='project-field']";
    private String ticketInputXpath = "//input[@id='issuetype-field']";
    private String assigneeInputXpath = "//input[@id='assignee-field']";
    private String summaryXpath = "//input[@id='summary']";

    @FindBy(xpath = "//input[@id='summary']")
    private WebElement summaryInput;

    @FindBy(xpath = "//textarea[@id='description']")
    private WebElement descriptionInput;

    @FindBy(xpath = "//input[@id='create-issue-submit']")
    private WebElement createButton;

    public CreateTicketForm(WebDriver d) {
        this.driver = d;
        PageFactory.initElements(driver, this);
    }

    public void selectProject(String projectKey, String projectName) {
        WebElement projectInput = Utils.waitForElementClickable(driver, projectInputXpath);
        projectInput.click();
        projectInput.sendKeys(projectKey);
        Utils.waitForElementClickable(driver, "//a[contains(@title, '" + projectName + "')]").click();
    }

    public void selectTicketType(String ticketKey, String ticketType) {
        WebElement ticketInput = Utils.waitForElementClickable(driver, ticketInputXpath);
        ticketInput.click();
        ticketInput.sendKeys(ticketKey);
        Utils.waitForElementClickable(driver, "//a[@title='" + ticketType + "']").click();
    }

    public void selectAssignee(String assigneeKey, String assigneeName) {
        WebElement assigneeInput = Utils.waitForElementClickable(driver, assigneeInputXpath);
        assigneeInput.click();
        assigneeInput.sendKeys(assigneeKey);
        Utils.waitForElementClickable(driver, "//a[@title='" + assigneeName + "']").click();
    }

    public void fillSummary(String summary) {
        Utils.waitForElementClickable(driver, summaryXpath).sendKeys(summary);
    }

    public void fillDescription(String description) {
        descriptionInput.sendKeys(description);
    }

    public TestProjectPage createTicket() {
        createButton.click();
        return new TestProjectPage(driver);
    }
}
