package test.java.jiratests.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import test.java.jiratests.utilities.Utils;

public class TestProjectPage {
    private final WebDriver driver;

    @FindBy(xpath = "//a[@class='aui-nav-link login-link']")
    private WebElement logInButton;

    @FindBy(xpath = "//a[@id='create_link']")
    private WebElement createButton;

    private String ticketLinkXpath = "//a[@class='issue-created-key issue-link']";


    public TestProjectPage(WebDriver d) {
        this.driver = d;
        PageFactory.initElements(driver, this);
    }

    public void open() {
        driver.get("https://jira.atlassian.com/");
    }

    public LogInPage clickLogInButton() {
        logInButton.click();
        return new LogInPage(driver);
    }

    public TestProjectPage loginAs(String login, String password) {
        this.open();
        LogInPage logInPage = this.clickLogInButton();
        return logInPage.loginAs(login, password);
    }

    public CreateTicketForm clickCreateButton() {
        createButton.click();
        return new CreateTicketForm(driver);
    }

    public TicketPage clickTicketLink() {
        Utils.waitForElementClickable(driver, ticketLinkXpath).click();
        return new TicketPage(driver);
    }
}
