package test.java.jiratests.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TicketPage {
    private final WebDriver driver;

    @FindBy(xpath = "//a[@id='project-name-val']")
    private WebElement projectName;

    @FindBy(xpath = "//span[@id='type-val']")
    private WebElement ticketType;

    @FindBy(xpath = "//span[@id='assignee-val']")
    private WebElement assigneeName;

    @FindBy(xpath = "//h1[@id='summary-val']")
    private WebElement summaryText;

    @FindBy(xpath = "//div[@id='description-val']//p")
    private WebElement descriptionText;

    public TicketPage(WebDriver d) {
        this.driver = d;
        PageFactory.initElements(driver, this);
    }

    public boolean verifySummary(String summary) {
        return summaryText.getText().equals(summary);
    }

    public boolean verifyDescription(String description) {
        return descriptionText.getText().equals(description);
    }

    public boolean verifyAssigneeName(String assignee) {
        return assigneeName.getText().equals(assignee);
    }

    public boolean verifyTicketType(String ticket) {
        return ticketType.getText().equals(ticket);
    }

    public boolean verifyProjectName(String project) {
        return projectName.getText().equals(project);
    }
}
