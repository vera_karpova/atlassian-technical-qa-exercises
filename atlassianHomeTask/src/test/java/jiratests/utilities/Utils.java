package test.java.jiratests.utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {
    public static final long WEB_TIMEOUT = 10;

    public static WebElement waitForElementClickable(WebDriver driver, String xpath) {
        WebDriverWait wait = new WebDriverWait(driver, WEB_TIMEOUT);
        return wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
    }
}
