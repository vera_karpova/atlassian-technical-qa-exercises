package test.java.jiratests.testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import test.java.jiratests.data.Ticket;
import test.java.jiratests.data.User;
import test.java.jiratests.pageobjects.CreateTicketForm;
import test.java.jiratests.pageobjects.TestProjectPage;
import test.java.jiratests.pageobjects.TicketPage;
import test.java.jiratests.utilities.Utils;

import java.util.concurrent.TimeUnit;

public class CreateJiraTask {
    private static WebDriver driver = null;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(Utils.WEB_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

    @Test
    public void success_CreateTicket() {
        TestProjectPage testProjectPage = new TestProjectPage(driver);
        User user = new User();
        Ticket ticket = new Ticket();

        // Login to Jira
        testProjectPage.loginAs(user.email, user.password);
        // Click Create button
        CreateTicketForm ticketForm = testProjectPage.clickCreateButton();
        // Select project
        ticketForm.selectProject(ticket.projectKey, ticket.projectName);
        // Select ticket type
        ticketForm.selectTicketType(ticket.ticketKey, ticket.ticketType);
        // Fill summary
        ticketForm.fillSummary(ticket.summary);
        // Select assignee
        ticketForm.selectAssignee(ticket.assigneeKey, ticket.assigneeName);
        // Fill description
        ticketForm.fillDescription(ticket.description);
        // Create ticket
        testProjectPage = ticketForm.createTicket();
        // Open ticket
        TicketPage ticketPage = testProjectPage.clickTicketLink();
        // Check ticket data
        Assert.assertTrue(ticketPage.verifySummary(ticket.summary), "Summary is wrong");
        Assert.assertTrue(ticketPage.verifyDescription(ticket.description), "Description is wrong");
        Assert.assertTrue(ticketPage.verifyProjectName(ticket.projectName), "Project Name is wrong");
        Assert.assertTrue(ticketPage.verifyTicketType(ticket.ticketType), "Ticket type is wrong");
        Assert.assertTrue(ticketPage.verifyAssigneeName(ticket.assigneeName), "Assignee is wrong");
    }
}
